/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, AsyncStorage } from 'react-native';
import Main from './src/Components/Main';
import { Provider } from 'react-redux';
import {KnongdaiCenstralStore} from './src/Stores/KnongdaiCenstralStore'
import OneSignal from 'react-native-onesignal'; // Import package from node modules of OneSignal
import { ActionType } from './src/Actions/ActionType';

export default class App extends Component {
  constructor(){
    super()
    this.state={
      oneSignalObj:{},
    }
  }

  componentWillMount(){
      // ------------------------ start Onesignal --------------------------------------//
      OneSignal.init(ActionType.ONESIGNAL_KEY, {kOSSettingsKeyAutoPrompt : true});

      OneSignal.addEventListener('received', this.onReceived);
      OneSignal.addEventListener('opened', this.onOpened);
      OneSignal.addEventListener('ids', this.onIds);
      OneSignal.configure();
      // ------------------------ end Onesignal --------------------------------------//
     
  }
  componentDidMount(){
    this.onIds();
  }

 

// ------------------------ start OneSignal ----------------------------------------//

componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);

}

onReceived = (notification) => {
    console.log("Notification received: ", notification);
}

onOpened = (openResult) => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
}

  onIds = async (device) => {
    // this.setState({
    //     oneSignalObj:device
    // })
    if(device!==undefined){
      try {
        await AsyncStorage.setItem('userPlayerId', device.userId);
     } catch (error) {
         console.log("Error saving data" + error);
     }
    }
   
}

// ------------------------ end OneSignal ------------------------------------------//



  render() {
    return (
      <Provider store={KnongdaiCenstralStore}>
        <Main />
      </Provider>
    );
  }
}


