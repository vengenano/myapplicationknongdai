//
//  NotificationService.h
//  OneSignalServiceExtension
//
//  Created by Chea Rothmonita on 2/1/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
