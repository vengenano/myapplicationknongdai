import ReactNative from 'react-native';
import I18n from 'react-native-i18n';

// Import all locales
import en from '../Locale/en.json';
import km from '../Locale/km.json';

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

// Define the supported translations
I18n.translations = {
  en,
  km
};



// chanage currentLocale to load locale (language) from LocalStorage
//I18n.locale = 'km'

let currentLocale = ''

export function chanageLanguage(lang) {
  if (lang === '') {
    currentLocale = I18n.currentLocale();
  } else {
    I18n.locale = lang
  }
}




// alert(currentLocale)

// Is it a RTL language?
export const isRTL = currentLocale.indexOf('km') === 0 || currentLocale.indexOf('ar') === 0;

// Allow RTL alignment in RTL languages
ReactNative.I18nManager.allowRTL(isRTL);

// The method we'll use instead of a regular string
export function strings(name, params = {}) {
  return I18n.t(name, params);
};

export default I18n;