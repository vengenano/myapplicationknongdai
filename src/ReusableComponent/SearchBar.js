import React, { Component } from 'react';
import { TextInput,Text, View, Image, StyleSheet ,Platform,TouchableOpacity} from 'react-native'
import { Button,Card,CardItem } from 'native-base'
import {Actions} from 'react-native-router-flux'
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import {fetchPopularUrl,fetchKeywordSuggestion} from '../Actions/KnongDaiAction';
import {connect} from 'react-redux'
import SearchableDropdown from 'react-native-searchable-dropdown'

class SearchBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchValue: this.props.initialValue,
            items :[],
            keywords:[]
        }
    }
    handleOnSearchButton(searchValue){
        console.log("Search is :",searchValue)
        const param = {
            searchValue: searchValue,
        }
        console.log("Search is :",this.state.searchValue)
        this.props.propHome&&Actions.result(param)
        
    }

    onChangeValue() {
        this.props.changeValue(this.state.searchValue);
        const param={
            searchValue: this.state.searchValue
        }
        this.props.propHome&& this.gotoReusultScreen(param)
    }
    onhandleItemClick=(keywordPop)=>{
        this.props.changeValue(keywordPop);
        const param={
            searchValue: keywordPop
        }
        this.props.propHome&& this.gotoReusultScreen(param)
        
    }
    gotoReusultScreen(param){
        this.setState({
            searchValue:" "
        })
        Actions.result(param)
    }
    componentWillMount(){
        this.props.propHome && this.setState({searchValue: ""})
    }

    componentDidMount(){
        this.props.fetchPopularUrl();
    }
    handleKeyDown(e){
        if(e.nativeEvent.key == "Enter"){
            dismissKeyboard();
        }
       
    }
    handleTextChange(){
        console.log('=====',this.state.searchValue)
        this.props.fetchKeywordSuggestion(this.state.searchValue)
        // this.setState({
        //     keywords:this.props.keywordSuggestion
        // })
        // console.log('Keyword Suggestiononkeypress==',this.state.keywords)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.keywordSuggestion !==this.props.keywordSuggestion){
            let keywords = nextProps.keywordSuggestion;
            let keywordObject = []
            for ( let i =0; i<keywords.length; i++){
                keywordObject.push({
                    name:keywords[i]
                })
            }
            this.setState({
                items:keywordObject
            })
        }
    }
   
    render() {
        const { popularUrl,keywordSuggestion } = this.props
       
        return (
            <View style={{width:'100%',marginBottom:5,position:'relative',borderBottomColor:'#eee',borderBottomWidth:this.props.propHome &&1}}>   
                <View style={[styles.searchbar]}>
                    <TextInput
                        onChangeText={(searchValue) => this.setState({ searchValue })}
                        style={[styles.searchbox,{marginLeft:this.props.propHome?25:15}]}
                        // value={this.props.action && this.props.searchValue}
                        value={this.state.searchValue}
                        //onChange={(event) => this.onHandleChange(event)}
                        //placeholder="ស្វែងរកនៅទីនេះ..." 
                        keyboardType="default"
                        returnKeyType="done"
                       // onKeyPress={(e)=>this.handleKeyDown(e)}
                        onKeyPress={()=>this.handleTextChange()}
                        />
                    <Button
                        //onPress={this.handleOnSearchButton(this.state.searchValue)}
                        onPress={this.onChangeValue.bind(this)}
                        style={styles.btnsearch}
                        transparent>
                        <Image
                            source={require('../Image/icons8-search-90.png')}
                            style={{ width: 25, height: 25 }}
                        />
                    </Button>
                </View>
               
                {/* start search suggestion dropdown */}
                <View
                    style={{flexDirection:'row-reverse',width:'95%',alignSelf:'center',margin:5,position:'relative',justifyContent:'center',borderRadius:30,borderWidth:1,borderColor:'rgba(0,0,0,0.1)'}}    
                >
                   <View style={{position:'absolute',left:5,zIndex:999,top:Platform.OS==='ios' ?8:12}}>
                   <TouchableOpacity
                    onPress={this.onChangeValue.bind(this)}
                   >
                        <Image
                        source={require('../Image/icons8-search-90.png')}
                        style={{ width: 25, height: 25 }}
                        />
                    </TouchableOpacity>
                   </View>
                    <View style={{flex:1}}>
                        <SearchableDropdown
                            onTextChange={
                                (searchValue)=>{
                                    this.setState({ searchValue: searchValue===""? " ": searchValue})
                                    this.props.fetchKeywordSuggestion(searchValue)
                                }
                            }
                            onItemSelect = {item=>this.onhandleItemClick(item.name)}
                            containerStyle={{ padding: 5,paddingLeft:10, }}
                            value={this.state.searchValue}
                            // onChange={(event) => this.onHandleChange(event)}
                            textInputStyle={{
                            padding: 10,
                            marginLeft:5,
                            borderBottomWidth: 1,
                            borderBottomColor: 'rgba(0,0,0,0)',  
                            borderRadius:10 ,
                            // color:'rgba(0,0,0,0)'                      
                            }}
                            itemStyle={{
                            padding: 12,
                            marginTop: 0,
                            color:'black',
                            backgroundColor: 'rgba(0,0,0,0)',
                            borderTopColor: 'rgba(0,0,0,0.1)',
                            borderTopWidth: 1,
                            width:'98.5%',
                            alignSelf:'center'
                            }}
                            itemTextStyle={{ color: '#555555' }}
                            itemsContainerStyle={{ maxHeight: 150,maxWidth:'100%'}}
                            items={this.state.items}
                        // defaultIndex={2}
                            placeholder="ស្វែងរកនៅទីនេះ..."
                            resetValue={false}
                            underlineColorAndroid="transparent"
                            containerStyle={{width:'100%'}}
                        />
                    </View>
                </View>
                {/* end search suggestion diopdown */}
                {
                    this.props.propHome ?
                    <View style={{height:50,width:'95%',alignSelf:'center'}}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <View style={styles.popularBox}>
                                {popularUrl.map((url) => (
                                    //{console.log("===>++",url.title)}
                                    <TouchableOpacity   
                                    onPress={()=>this.onhandleItemClick(url.title)}                                 
                                    style={styles.txtBox} 
                                    key={url.id}>
                                        <Text 
                                        style={styles.txtStyle}
                                        >{url.title}</Text>
                                    </TouchableOpacity>  
                                  ))}                                                           
                            </View>
                        </ScrollView> 
                    </View>
                          
                    :
                    <View></View>
                }          
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {      
        demo:'124',
        popularUrl:state.knongdais.knongDaiPopularUrl,
        keywordSuggestion:state.knongdais.keywordSuggestion
    }
}
export default connect(mapStateToProps, { fetchPopularUrl,fetchKeywordSuggestion })(SearchBar);

const styles = StyleSheet.create({
    searchbar: {
        width: '95%',
        alignSelf:'center',
        flexDirection: 'row',
        borderWidth: 0,
        borderColor: 'rgba(0,0,0,0.1)',
        margin: 6,
        marginBottom:0,
        borderRadius: 25,
        backgroundColor:'white',
        //zIndex:10000,
        display: 'none',
        position:'absolute'
    },
    searchbox: {
        flex: 6, marginLeft:25,color:'white'
    },
    btnsearch: {
        alignSelf: 'flex-end', marginRight: 10,
        opacity:0
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.2,
        shadowRadius: 5,
    },
    popularBox:{
        flexDirection:'row',
        flexWrap:'wrap', 
        backgroundColor:'white',
        padding:-15,
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
        marginTop: -40,
        marginBottom:-35,
    },
    txtBox:{
        backgroundColor:'rgb(62, 127, 131)',
        marginHorizontal:5,
        paddingHorizontal: 8,
        borderRadius:20,
        paddingVertical: 10,
    },
    txtStyle:{color:'white',fontSize:10}
})
