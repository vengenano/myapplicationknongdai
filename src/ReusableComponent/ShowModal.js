import React, { Component } from 'react';
import { View, Text ,Platform,TouchableHighlight,Dimensions,TouchableOpacity} from 'react-native';
import Modal from 'react-native-modalbox'

var screen = Dimensions.get('window')

export default class ShowModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data:[
            {id:1,name:'sport'},
            {id:2,name:'sport1'},
            {id:3,name:'sport2'},
            {id:4,name:'sport3'},
            {id:5,name:'sport4'},
        ]
    };
  }
  showModal=()=>{
       this.refs.myModal.open()
  }
  render() {
    return (
        <Modal
            ref={"myModal"}
            style={{
                justifyContent:'center',
                borderRadius: Platform.OS==='ios'? 30:0,
                shadowRadius:10,
                width:screen.width-80,
                height:200 
            }}
            position='center'
            backdrop={true}
            onClosed={()=>{
                alert("Modal Closed")
            }}
        >
            <Text>Title</Text>
        </Modal>
    );
  }
}
