import React, { Component } from 'react';
import {Header,Left,Button,Body,Title} from 'native-base'
import {Image} from 'react-native'
import { strings } from './../Language/ActionSwitch/i18n';

class HeaderBar extends Component {
    render() {
        return (
            <Header style={{backgroundColor:'#F7941D'}}>
                <Left>
                <Button transparent>
                <Image
                    style={{ marginLeft:5,width: 20, height: 20, }}
                    source={require('../Image/icons8-left-100-white.png')}
                            />
                </Button>
                </Left>
                <Body>
                <Title style={{color:'white',fontSize:20}}>{this.props.title}</Title>
                </Body>
          </Header>
        );
    }
}

export default HeaderBar;