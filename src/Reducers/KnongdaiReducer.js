import { ActionType } from '../Actions/ActionType'

const initState = {
    pagination: 0,
    knongdai: [],
    knongdaiSubCategory: [],
    knongdaiResult: [],
    keywordSuggestion: [],
    knongDaiPopularUrl: [],
    status: false,
    isCreated: false,
    isCreateUser: false,
    isUserUpdate: false,
    knongdaiUser: []
}
export const knongdaiReducer = (state = initState, action) => {
    switch (action.type) {
        case ActionType.KNONGDAI_FETCH_MAIN_CATEGORY:
            return { ...state, knongdai: action.payload.data }

        case ActionType.KNONGDAI_FETCH_SUB_CATEGORY:
            return { ...state, knongdaiSubCategory: action.payload.data }

        case ActionType.KNONGDAI_FETCH_KEYWORD_RESULT:
            return { ...state, knongdaiResult: action.payload }

        case ActionType.KNONGDAI_FETCH_KEYWORD_SUGGESTION:
            return { ...state, keywordSuggestion: action.payload }

        case ActionType.KNONGDAI_FETCH_POPULAR_URL:
            return { ...state, knongDaiPopularUrl: action.payload }

        case ActionType.KNONGDAI_POST_URL_REQUEST:
            return { ...state, isCreated: action.payload.status }

        case ActionType.KNONGDAI_CREATE_USER:
            return { ...state, isCreateUser: action.payload.status }

        case ActionType.KNONGDAI_FETCH_USER_BY_FACEBOOKID:
            return { ...state, knongdaiUser: action.payload.data }
            
        case ActionType.KONGDAI_UPDATE_USER_BY_FACEBOOK_ID:
            return { ...state, isUserUpdate: action.payload.status }
        default:
            return state
    }
    // return state
}
