import {ActionType} from "./ActionType"
export const  fetchMainCategory = () => {
    return(dispatch)=>{
        fetch(ActionType.BASE_RUL+'/api/v1/categories')
        .then(res=>res.json())
        .then(res=>
        {
            console.log('========',res)
            dispatch({
            type:ActionType.KNONGDAI_FETCH_MAIN_CATEGORY,
            payload:res
        })})
    }
}

export const fetchSubCategoryByMainId = (id) =>{
    return(dispatch)=>{
        fetch(ActionType.BASE_RUL+'/api/v1/categories/sub-by-main-id/'+id)
        .then(res=>res.json())
        .then(res=>{
            dispatch({
                type:ActionType.KNONGDAI_FETCH_SUB_CATEGORY,
                payload:res
            })
        })
    }
}

export const fetchResult = (keyword,subCateId,page) =>{
    console.log("===> page: ", page)
    return(dispatch)=>{
        fetch(//keyword? ActionType.BASE_RUL+'/api/v1/search?q='+keyword+'&page=1':
            subCateId ? ActionType.BASE_RUL+'/api/v1/search?cate_id='+subCateId+'&page='+page:
            keyword && ActionType.BASE_RUL+'/api/v1/search?q='+keyword+'&page='+page
        )
        .then(res=>res.json())
        .then(res=>{
            dispatch({
                type:ActionType.KNONGDAI_FETCH_KEYWORD_RESULT,
                payload:res
            })
        })
    }
}
export const fetchKeywordSuggestion = (keyword) =>{
    return(dispatch)=>{
        fetch(ActionType.BASE_RUL+'/api/v1/search/keyword/'+keyword)
        .then(res=>res.json())
        .then(res=>{
            dispatch({
                type:ActionType.KNONGDAI_FETCH_KEYWORD_SUGGESTION,
                payload:res
            })
        })
    }
}
export const fetchPopularUrl = () =>{
    return(dispatch)=>{
        fetch(ActionType.BASE_RUL+'/api/v1/urls/popular')
        .then(res=>res.json())
        .then(res=>
        {
            dispatch({
            type:ActionType.KNONGDAI_FETCH_POPULAR_URL, 
            payload:res
        })})
    }
}

export const createUrl = (url
                         ) => {
    return (dispatch) => {
        fetch(ActionType.BASE_RUL+'/api/v1/urls/create', {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': API.key
            },
            method: 'POST',
            body: JSON.stringify({
                title:  url.title,
                type:   url.type,
                link:   url.link,
                phone:  url.phone,
                email:  url.email,
                address: url.address,
                pic_url: url.pic_url,
                sub_cate_id: url.sub_cate_id,
                keywords: url.keywords,
                des:  url.description,
                userId: url.userId,
            })
        }).then(r => r.json())
            .then(res => dispatch({
                type: ActionType.KNONGDAI_POST_URL_REQUEST,
                payload: res
            })).catch(err => console.log(err))
    }
}
export const createUser = (user) => {
    return (dispatch) => {
        fetch(ActionType.BASE_RUL+'/api/v1/users/register/create', {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': API.key
            },
            method: 'POST',
            body: JSON.stringify({
                facebookId: user.facebookId,
                gender: user.gender,
                phoneNumber: user.phoneNumber,
                userName: user.userName,
                userPlayerId: user.userPlayerId
            })
        }).then(r => r.json())
            .then(res => dispatch({
                type: ActionType.KNONGDAI_CREATE_USER,
                payload: res
            })).catch(err => console.log(err))
    }
}
export const fetchUserByFacebookId = (facebookId) =>{
    return(dispatch)=>{
        fetch(ActionType.BASE_RUL+'/api/v1/users/register/'+facebookId)
        .then(res=>res.json())
        .then(res=>
        {
            dispatch({
            type:ActionType.KNONGDAI_FETCH_USER_BY_FACEBOOKID, 
            payload:res
        })})
    }
}
export const updateUser = (facebookId,userName,gender) => {
    return (dispatch) => {
        fetch('http://35.240.224.58:28080/api/v1/users/register/update', {
            headers: {
                'Content-type': 'application/json'
                // 'Authorization': 'Basic QVBJX1VTRVJOQU1FOkFQSV9QQVNTV09SRA=='
            },
            method: 'PUT',
            body: JSON.stringify({
                facebookId:facebookId,
                userName:userName,
                gender:gender
            })
        }).then(r => r.json())
            .then(res => dispatch({
                type: ActionType.KONGDAI_UPDATE_USER_BY_FACEBOOK_ID,
                payload: res
            }))
    }
}


