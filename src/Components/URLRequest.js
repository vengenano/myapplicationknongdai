import React, { Component } from 'react';
import { View, Text,StyleSheet, } from 'react-native';
import {Card} from 'native-base'
import URLRequestInput from './URLRequestInput'
import {strings,chanageLanguage} from '../Language/ActionSwitch/i18n.js'
import I18n from 'react-native-i18n'

export default class URLRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: I18n.locale

    };
  }
  componentWillUpdate() {
    AsyncStorage.getItem("language")
    .then(lang => {
        chanageLanguage(lang)
        this.setState({language: lang})
    })
}
  render() {
    return (
      <View style={styles.containerfluid}>
        <Card>
          <Text style={styles.title}>{strings('urlrequest.webinfo')}</Text>
         
        </Card> 
        <URLRequestInput />       
      </View>
    );
  }
}

const styles=StyleSheet.create({
    containerfluid:{
        flex:1     
    },
    title:{
        fontSize:20,
        color:'#F7941D',
        fontFamily: 'Roboto',
        alignSelf: 'center',
        marginBottom: 10,
    }
})
