import React, { Component } from 'react';
import { View, Text ,TouchableOpacity
    ,StyleSheet,
    Platform,
    Image,
    AsyncStorage,
} from 'react-native';
import {Actions} from 'react-native-router-flux'
import RNAccountKit from 'react-native-facebook-account-kit'
import Axios from 'axios';
import {strings,chanageLanguage} from '../Language/ActionSwitch/i18n.js'
import I18n from 'react-native-i18n'

export default class NoUserScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: I18n.locale ,
    };
  }
  componentWillUpdate() {
      AsyncStorage.getItem("language")
      .then(lang => {
          chanageLanguage(lang)
          this.setState({language: lang})
      })
  }
  sendRequestForPhoneNumber = async (token) => {

    try {
      var access_token_data = ['AA', '304340463524448', '9010ea6a6b5cbd6e36d16ed0da0cb698']

      var access_token = access_token_data.join('|');

      var token_exchange_url = `https://graph.accountkit.com/v1.1/access_token?grant_type=authorization_code&code=${token}&access_token=${access_token}`;
      let getTokens = await Axios(token_exchange_url);
      let getDetailsUrl = `https://graph.accountkit.com/v1.1/me?access_token=${getTokens.data.access_token}`;

      let getDetailsUser = await Axios(getDetailsUrl);
      return getDetailsUser.data;
    } catch (error) {
      console.log('from the catch block: ', error);
    }
  }
  handleOnLogin() {
    RNAccountKit.configure({
      responseType: 'code', // 'token' by default,
      titleType: 'login',
      initialPhoneCountryPrefix: '+855', 
      receiveSMS: true, 
      defaultCountry: 'KM',
      getACallEnabled: true
    })
    RNAccountKit.loginWithPhone()
      .then(async (token) => {
        if (!token) {
          console.log('Login cancelled')
        } else {
          console.log(`Log with phone ${JSON.stringify(token)} `)
          var getResults = await this.sendRequestForPhoneNumber(token.code);
          const param = {
            facebookId: getResults.id,
            phoneNumber: getResults.phone.number
          }
        Actions.register(param)
        }
      })

  } 
  render() {
    return (
      <View style={styles.constainer}>
        <View style={styles.imgBox}>
            <Image
                source={require('../Image/logo-KD-small.png')}
                style={styles.img}
            />
        </View>
        <Text style={styles.txt}>{strings('nouserscreen.nouser')}</Text>
        <TouchableOpacity
            style={styles.btn}
            onPress={()=>Platform.OS==='ios'? Actions.home(): this.handleOnLogin()}
        >
            <Text style={styles.txtBtn}>{strings('nouserscreen.continue')}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    constainer:{
        flex:1,
        paddingTop: Platform.OS==='ios'? 120:60,
        alignItems: 'center',
        justifyContent:"flex-start",
        backgroundColor:'white'
    },
    btn:{
        backgroundColor: '#3E7F83',
        paddingHorizontal: 40,
        borderRadius: 25,
        marginTop: 20,
    },
    txtBtn:{
        textAlign:'center',
        color:'white',
        padding:10,
        fontWeight: 'bold',
        fontSize:13,
        paddingVertical: 15,
    },
    imgBox:{
        width:120,
        height:120,
    },
    img:{
        width:'100%',
        height:'100%'
    },
    txt:{
        marginVertical:30,
        fontSize: 16,
    }
})