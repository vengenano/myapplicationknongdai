import React, { Component } from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";

export default class SearchSuggetion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: []
    };
  }

  componentDidMount() {
    const url = 'http://www.knongdai.com/api/v1/search/keyword/s'
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          dataSource: responseJson
        })
      })
      .catch((error) => {
        console.log(error)
      })
  }

  renderItem = ({ item }) => {
    <View>
      <Text>{item}</Text>
    </View>
  }

  render() {
    // console.log("===>",this.state.dataSource)
    return (
      <View>
        <FlatList
          data={[{ key: 'a' }, { key: 'b' }]}
          renderItem={({ item }) => <Text>{item.key}</Text>}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  }
})
