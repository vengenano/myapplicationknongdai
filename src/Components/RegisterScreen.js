import React, { Component } from 'react';
import { createUser } from '../Actions/KnongDaiAction';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux'
import OneSignal from 'react-native-onesignal'; // Import package from node modules of 
import {
    View, Text,
    StyleSheet, Image,
    TouchableWithoutFeedback,
    StatusBar,
    TextInput,
    SafeAreaView,
    Keyboard, TouchableOpacity,
    KeyboardAvoidingView,
    Dimensions,
    Platform,
    AsyncStorage,
    ImageBackground,
    PixelRatio
} from 'react-native';
import { strings, chanageLanguage } from '../Language/ActionSwitch/i18n.js'
import I18n from 'react-native-i18n'
import {
    Label,
} from 'native-base';
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";


var { height, width } = Dimensions.get('window')
const isSmallDevice = PixelRatio.get() <= 2

class RegisterScreen extends Component {
    static navigationOptions = {
        header: null,
    }

    componentWillUpdate() {
        AsyncStorage.getItem("language")
            .then(lang => {
                chanageLanguage(lang)
                if (this.isMounted()) { // This is bad.
                    this.setState({ language: lang, language: I18n.locale })
                }

            })
    }

    constructor(props) {
        super(props);
        this.state = {
            facebookId: this.props.facebookId,
            phoneNumber: this.props.phoneNumber,
            userName: '',

            userPlayerId: '',
            // userPlayerId: 'b30b0b36-d25d-4f4a-b7d0-e93014751e11'
            gender: 'F',
            text: ''
        };
        this.onSelect = this.onSelect.bind(this)
    }
    async saveUser(facebookId, phoneNumber, userName, gender) {
        //here you store two texts with two different key or concatenate text1 and text2 
        //store the both texts with one key. 
        try {
            await AsyncStorage.setItem('facebookId', facebookId);
            await AsyncStorage.setItem('phoneNumber', phoneNumber);
            await AsyncStorage.setItem('userName', userName);
            await AsyncStorage.setItem('gender', gender);

        } catch (error) {
            console.log("Error saving data" + error);
        }
    }
    funUpdateState = (res) => {
        this.setState({
            userPlayerId: res
        })
        // console.log('userPlayerId after update  did mount :', this.state.userPlayerId)
    }
    handleOneSignal = () => {
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);

        OneSignal.getPermissionSubscriptionState((status) => {
            status.userId && this.setState({
                userPlayerId: status.userId
            })
            try {
                AsyncStorage.setItem('userPlayerId', this.state.userPlayerId);
            } catch (error) {
                console.log("Error saving data" + error);
            }
        });
    }
    // -------------------- start OneSignal ----------------------- //
    componentWillUnmount() {
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('ids', this.onIds);
    }
    onReceived(notification) {
        // console.log("Notification received: ", notification);
    }

    onOpened(openResult) {
        // console.log('Message: ', openResult.notification.payload.body);
        // console.log('Data: ', openResult.notification.payload.additionalData);
        // console.log('isActive: ', openResult.notification.isAppInFocus);
        // console.log('openResult: ', openResult);
    }

    onIds(device) {
        // console.log('Device info: ', device);
    }
    // -------------------- end OneSignal ----------------------- //

    componentWillMount() {
        OneSignal.init("b545bdf6-3322-4452-8af2-c60dbb3006a1");
        OneSignal.addEventListener('ids', this.onIds);
        this.handleOneSignal();
        AsyncStorage.getItem('userPlayerId')
        .then(res => this.funUpdateState(res))
    }
    getUser = async () => {
        try {
            const facebookId = await AsyncStorage.getItem('facebookId');
            const phoneNumber = await AsyncStorage.getItem('phoneNumber');
            const userName = await AsyncStorage.getItem('userName');
            const gender = await AsyncStorage.getItem('gender');
            this.setState({ facebookId, phoneNumber, userName, gender });

        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }

    handleOnRegister() {
        this.props.createUser(this.state)
        this.saveUser(this.state.facebookId, this.state.phoneNumber, this.state.userName, this.state.gender)
        this.getUser()
        Actions.drawer({ type: "reset" })
        Actions.home({ type: "reset" })
        Actions.drawerClose();
        Actions.home();
    }
    onSelect(index, name) {
        this.setState({
            gender: name,
        })
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="dark-content" />
                <KeyboardAvoidingView style={styles.container}>
                    <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss} >
                        <View style={styles.container}>
                            <ImageBackground source={require('../Image/Search.png')}
                                style={{ width: '100%', height: '100%' }}
                            >
                                <View style={styles.logoContainer}>
                                    <Image
                                        style={styles.logo}
                                        source={require('../Image/kd-circle.png')}
                                    />
                                    <Text style={styles.title}>{strings('register.register')}</Text>
                                    {/* <Text style={styles.title}>FacebookID : {this.props.facebookId}</Text> */}
                                    <Text style={styles.phoneNum}>{strings('register.phone')} : {this.props.phoneNumber}</Text>
                                </View>
                                <View style={[styles.infoContainer]}>

                                    <TextInput style={styles.input}
                                        placeholder='UserName '
                                        placeholderTextColor='rgba(255,255,255,0.8)'
                                        keyboardType='default'
                                        returnKeyType='next'
                                        autoCorrect={false}
                                        ref={'userName'}
                                        onChangeText={(userName) => this.setState({ userName })}
                                        // onSubmitEditing={() => this.refs.gender.focus()}
                                        autoCapitalize="none"
                                    />
                                    {/* <TextInput style={styles.input}
                                        placeholder='Gender'
                                        placeholderTextColor='rgba(255,255,255,0.8)'
                                        keyboardType='default'
                                        returnKeyType='go'
                                        autoCorrect={false}
                                        onChangeText={(gender) => this.setState({ gender })}
                                        ref={'gender'}
                                        autoCapitalize="none"
                                    /> */}
                                    <Label style={styles.des}>{strings('inputuser.gender')}</Label>

                                    <RadioGroup
                                        thickness={2}
                                        color='whitesmoke'
                                        //highlightColor='orange'
                                        selectedIndex={0}
                                        onSelect={(index, name) => this.onSelect(index, name)}
                                        style={styles.ratio}>
                                        <RadioButton value={"female"} name={"F"} style={styles.frm} color='orange'>
                                            <Text style={{ color: 'white' }}>{strings('inputuser.female')}</Text>
                                        </RadioButton>

                                        <RadioButton value={"male"} name={"M"} style={styles.frm} color='orange'>
                                            <Text style={{ color: 'white' }}>{strings('inputuser.male')}</Text>
                                        </RadioButton>
                                    </RadioGroup>

                                    <TouchableOpacity style={styles.buttonContainer}
                                        onPress={() => this.handleOnRegister()}
                                    >
                                        <Text style={styles.buttonText}>{strings('register.btnregister')}</Text>
                                    </TouchableOpacity>
                                </View>
                            </ImageBackground>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isCreateUser: state.knongdais.isCreateUser,
    }
}
export default connect(mapStateToProps, { createUser })(RegisterScreen);
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3E7F83',
        flexDirection: 'column',
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
    },
    logo: {
        width: 128,
        height: 128,
    },
    title: {
        color: 'white',
        fontSize: 16,
        textAlign: 'center',
        marginTop: 5,
        opacity: 0.9
    },
    infoContainer: {
        height: 200,
        padding: 20,
        paddingTop: 0,
    },
    input: {
        marginVertical: 10,
        height: 40,
        backgroundColor: 'rgba(0,0,0,0.2)',
        paddingHorizontal: 10,
        color: '#fff'
    },
    buttonContainer: {
        backgroundColor: '#F7941D',
        paddingVertical: 10,
        marginVertical: 10,
        borderRadius: 20,
        marginBottom: 5,
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 15,
    },
    phoneNum: {
        color: 'white',
        fontSize: 11,
        textAlign: 'center',
        marginTop: 5,
        opacity: 0.9,
    },
    frm: {
        fontSize: 16
    },
    ratio: {
        flex: 1,
        flexDirection: "row",
        marginLeft: 10,
        height: 40,
    },
    des: {
        marginTop: 5,
        fontFamily: "Roboto",
        fontSize: 16,
        color: 'white'
    },
})