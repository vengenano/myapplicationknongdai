import { createAppContainer, createDrawerNavigator, createStackNavigator } from 'react-navigation'
import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, Platform,AsyncStorage } from 'react-native'
import { Button } from 'native-base'
import { Router, Scene, Actions } from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import {strings,chanageLanguage} from '../Language/ActionSwitch/i18n.js'

import Home from './Home'
import SideMenu from './SideMenu'
import Setting from './Setting'
import AboutUS from './AboutUs'
import Policy from './Policy'
import Notification from './Notification'
import UserProfile from './UserProfile'
import URLRequest from './URLRequest'
import EditUser from './EditUser'
import ModalScreen from './ModalScreen'
import Result from "./Result"
import WebViewResult from "./WebViewResult"
import Splash from './Splash';
import RegisterScreen from './RegisterScreen';
import NoUserScreen from './NoUserScreen';

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
        language: I18n.locale
    };
  }
  componentWillUpdate() {
    AsyncStorage.getItem("language")
    .then(lang => {
        chanageLanguage(lang)
        this.setState({language: lang})
    })
  }
  render() {
    return (
      <Router navigationBarStyle={{ backgroundColor: '#F7941D' }}>
        <Scene key='root'>
          <Scene
            key='drawer'
            drawer
            contentComponent={SideMenu}
            drawerWidth={300}
            renderLeftButton={() =>
              <Button transparent onPress={() => Actions.drawerOpen()}>
                <Image source={require('../Image/humburger.png')}
                  style={{ width: 25, height: 25, marginLeft: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
              </Button>}
            renderRightButton={() =>
              <Button transparent onPress={() => Actions.notification()}>
                <Image source={require('../Image/icons8-alarm-filled-100.png')}
                  style={{ width: 25, height: 25, marginRight: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
              </Button>}
            hideNavBar
            initial
            type="reset"
          >
            <Scene key="home"
              renderTitle={() => (
                <View>
                  <Image source={require('../Image/logo-kd-txt.png')} style={{ width: 125, height: 15 }} />
                </View>
              )}
              titleStyle={{ color: 'white', fontSize: 20, }}
              component={Home}
              initial
            />
            <Scene
              key="setting"
              title={strings('sidemenu.setting')}
              titleStyle={{ color: 'white', fontSize: 20 }}
              component={Setting}
              back
              direction="verticle"
              renderBackButton={() =>
                <Button transparent onPress={() => Actions.home()}>
                  <Image source={require('../Image/icons8-left-100-white.png')}
                    style={{ width: 20, height: 20, marginLeft: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
                </Button>
              }
              renderRightButton={() =>
                <Button transparent></Button>
              }
             
            />
          </Scene>
          <Scene
            key="notification"
            title="Notification"
            titleStyle={{ color: 'white', fontSize: 20 }}
            component={Notification}
            back
            renderBackButton={() =>
              <Button transparent onPress={() => Actions.home()}>
                <Image source={require('../Image/icons8-left-100-white.png')}
                  style={{ width: 20, height: 20, marginLeft: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
              </Button>
            }
            renderRightButton={() =>
              <Button transparent></Button>
            }
          />

          <Scene
            key="policy"
            title="Policy"
            component={Policy}
            titleStyle={{ color: 'white', fontSize: 20 }}
            back
            renderBackButton={() =>
              <Button transparent onPress={() => Actions.pop()}>
                <Image source={require('../Image/icons8-left-100-white.png')}
                  style={{ width: 20, height: 20, marginLeft: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
              </Button>
            }
            renderRightButton={() =>
              <Button transparent></Button>
            }
          />
          <Scene
            key="about"
            title="About Us"
            component={AboutUS}
            titleStyle={{ color: 'white', fontSize: 20 }}
            back
            renderBackButton={() =>
              <Button transparent onPress={() => Actions.pop()}>
                <Image source={require('../Image/icons8-left-100-white.png')}
                  style={{ width: 20, height: 20, marginLeft: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
              </Button>
            }
            renderRightButton={() =>
              <Button transparent></Button>
            }
          />
          <Scene
            key="userprofile"
            title="User Profile"
            component={UserProfile}
            titleStyle={{ color: 'white', fontSize: 20 }}
            back
            navigationBarStyle={{borderBottomWidth: 0,shadowOpacity: 0,backgroundColor:'#F7941D'}}
            renderBackButton={() =>
              <Button transparent onPress={() => Actions.pop()}>
                <Image source={require('../Image/icons8-left-100-white.png')}
                  style={{ width: 20, height: 20, marginLeft: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
              </Button>
            }
            renderRightButton={() =>
              <Button transparent></Button>
            }
          />
          <Scene
            key="urlrequest"
            title="URL Request"
            component={URLRequest}
            titleStyle={{ color: 'white', fontSize: 20 }}
            back
            renderBackButton={() =>
              <Button transparent onPress={() => Actions.pop()}>
                <Image source={require('../Image/icons8-left-100-white.png')}
                  style={{ width: 20, height: 20, marginLeft: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
              </Button>
            }
            renderRightButton={() =>
              <Button transparent></Button>
            }
          />
          <Scene
            key="edituser"
            title="Edit User Profile"
            component={EditUser}
            titleStyle={{ color: 'white', fontSize: 20 }}
            back
            renderBackButton={() =>
              <Button transparent onPress={() => Actions.pop()}>
                <Image source={require('../Image/icons8-left-100-white.png')}
                  style={{ width: 20, height: 20, marginLeft: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
              </Button>
            }
            renderRightButton={() =>
              <Button transparent></Button>
            }
          />
          <Scene
            key="result"
            title="Result"
            component={Result}
            titleStyle={{ color: 'white', fontSize: 20 }}
            back
            hideNavBar
          />
           <Scene
            key="webview"
            title="WebView"
            component={WebViewResult}
            titleStyle={{ color: 'white', fontSize: 20 }}
            back
            hideNavBar
          />
          <Scene
            key="modal"
            component={ModalScreen}
            title="Modal"
            modal
            direction='verticle'
            hideNavBar
          />
          <Scene
            key="splash"
            component={Splash}
            direction='verticle'
            hideNavBar
            initial
          />
          <Scene
            key="nouser"
            component={NoUserScreen}
            hideNavBar
            back
          />
          <Scene
            key="register"
            title='Setting'
            titleStyle={{ color: 'white', fontSize: 20 }}
            component={RegisterScreen}
            back
            renderBackButton={() =>
              <Button transparent onPress={() => Actions.home()}>
                <Image source={require('../Image/icons8-left-100-white.png')}
                  style={{ width: 20, height: 20, marginLeft: 15, marginTop: Platform.OS == 'android' ? 10 : 0 }} />
              </Button>
            }
            renderRightButton={() =>
              <Button transparent></Button>
            }
          />
        </Scene>
      </Router>
    )
  }
}