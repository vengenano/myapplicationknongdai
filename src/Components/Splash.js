import React, { Component } from 'react';
import {View,Text,Image,Platform,StyleSheet,Animated,Dimensions,AsyncStorage} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {strings,chanageLanguage} from '../Language/ActionSwitch/i18n.js'
import I18n from 'react-native-i18n'

var {height,width} = Dimensions.get('window')
const styles=StyleSheet.create({
    container:{
        paddingTop:Platform.OS==='ios'? 40:0 ,
        flex:1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'white',
    },
    logoStyle:{
        width:140,height:140,
    },
    txtStyle:{
        fontSize:16,
        fontWeight: 'bold',
        color:'#F7941D',
        textShadowColor: 'rgba(0, 0, 0, 0.03)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    },
    txtBottom:{
        fontSize:12,
        textAlign:'center',
        color:'#444444'
    },
    txtBottomContainer:{
        position:'absolute',flex:1,
        bottom:Platform.OS==='android'? 5:20
    }
})
export default class Splash extends Component {
    static navigationOptions = {
        header : null,
    }
    state={
        animateOpcity: new Animated.Value(0),
        titleMarginTop: new Animated.Value(height/3),
        language: I18n.locale
    }
    componentWillUpdate() {
        AsyncStorage.getItem("language")
        .then(lang => {
            chanageLanguage(lang)
            this.setState({language: lang})
        })
    }

    async componentDidMount(){
        //Add animations here
        Animated.sequence([
            //animations by sequence
            Animated.timing(this.state.animateOpcity,{
                toValue:1,
                duration:1700
            }),
            //animate text
            Animated.timing(this.state.titleMarginTop,{
                toValue:10,
                duration:1000
            })

        ]).start(()=>{
            //navigate to next screen here
            Actions.home()
        })
    }

    render() {
        return (
            <View style={styles.container}>
               <Animated.Image
                source={require('../Image/logo-KD-small.png')}
                style={[styles.logoStyle,{opacity:this.state.animateOpcity}]}
              / >
                <Animated.Text style={[styles.txtStyle,{marginTop:this.state.titleMarginTop}]}>
                    {strings('splash.slogan')}
                </Animated.Text>
                <View style={styles.txtBottomContainer}>
                    <Text style={[styles.txtBottom]}>
                    {strings('splash.web')}
                    </Text>
                </View>
            </View>
        )
    }
}
