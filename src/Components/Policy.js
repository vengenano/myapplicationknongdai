import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';
import {StyleSheet} from 'react-native';
import ImageLogo from './imageLogo.js';
import {strings,chanageLanguage} from '../Language/ActionSwitch/i18n.js'
import I18n from 'react-native-i18n'
export default class Policy extends Component {
  state={language: I18n.locale}

  componentWillUpdate() {
    AsyncStorage.getItem("language")
    .then(lang => {
        chanageLanguage(lang)
        this.setState({language: lang})
    })
}
  render() {
    return (
      <Container style={{backgroundColor:'#F2F2F0'}}>
        <Content>
          <ImageLogo />    
          <Card >
            <CardItem>
              <Body>
              <Text style={styles.myCardTitle}>{strings('policy.thank')} </Text>
                <Text style={styles.introductionCard}>
                {strings('policy.introduction')}
                </Text>
              </Body>
            </CardItem>
          </Card> 
        </Content>

        
      </Container>
    );
  }
}
const styles=StyleSheet.create({
  myStyle:{
      // fontWeight: 'bold'
      fontSize:15,
      color:'#555555'

  },
  
  spaceBetween:{
      marginLeft:35,
      fontSize:15,
      color:'#555555'
  },

  myCardTitle:{
    color:'#3E7F83',
    fontSize:19,
    fontWeight: 'bold',


  },
  myCard:{
   padding:5,
  
  },

})