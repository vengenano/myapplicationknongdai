import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Button, Picker, Icon, Textarea } from 'native-base';

import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import ImagePicker from 'react-native-image-picker';
import { createUrl } from '../Actions/KnongDaiAction';
import { connect } from 'react-redux';
import { ActionType } from '../Actions/ActionType';
import {strings,chanageLanguage} from '../Language/ActionSwitch/i18n.js'
import I18n from 'react-native-i18n'
const options = {
  title: 'URL Request',
  takePhotoButtonTitle: 'Take photo with your camera',
  chooseFromLibraryButtonTitle: 'Choose photo from your gallery'
}
class URLRequestInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      type: '',
      link: '',
      phone: '',
      email: '',
      address: '',
      pic_url: '/resources/upload/4679fc9f-672c-4863-9d71-fbefd1f1d097.png',
      sub_cate_id: 4,
      keyword: '',
      description: '',
      userId: 20,
      pickedImage: null,
      data: null,
       language: I18n.locale
    };
  }
 
  componentWillUpdate() {
    AsyncStorage.getItem("language")
    .then(lang => {
        chanageLanguage(lang)
        this.setState({language: lang})
    })
}

imgClick = () =>{
  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);
  
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = { uri: response.uri };
  
      // You can also display the image using data:
      // const source = { uri: 'data:image/jpeg;base64,' + response.data };
  
      this.setState({
        avatarSource: source,
      });
    }
  });
}
  
  selectHandle = (index,i) =>{
    if(index ==0){
      this.setState({
        type: 'w'
      })
    } else if (index == 1) {
      this.setState({
        type: 'f'
      })
    }

  }
  pickImageHandler = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
      if (res.didCancel) {
        console.log("User cancelled!");
      } else if (res.error) {
        console.log("Error", res.error);
      } else {
        let source = { uri: res.uri }
        this.setState({
          pickedImage: source,
          data: res.data
        });

        console.log(this.state.pickedImage.uri)
      }
    });
  }
  uploadPhoto() {
    RNFetchBlob.fetch('POST', 'http://localhost:9090/api/v1/urls/upload/web-icon', {
      Authorization: "Bearer access-token",
      otherHeader: "foo",
      'Content-Type': 'multipart/form-data',
    }, [
        // custom content type
        { name: 'image-png', filename: 'image-png.png', type: 'image/png', data: this.state.data },
        // part file from storage              
      ]).then((resp) => {
        // ...
      }).catch((err) => {
        // ...
      })
  }
  funcAddUrl = () => {
    // e.preventDefault()
    console.log('Create URL Prop first Save :', this.props)
    this.props.createUrl(this.state)
    console.log('Create URL Prop After Save :', this.props)
  }

  componentDidMount(){
    const facebookId = "";
    AsyncStorage.getItem('facebookId')
    .then(res=> alert(this.props.fetchUserByFacebookId(res)))
  }

  render() {
    return (
      <Container>
        <Content>
          <Form style={[styles.frm, { padding: 5 }]}>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>{strings('urlrequestinput.webname')}</Label>
              <Input placeholder={strings('urlrequestinput.blurweb')} style={styles.all}  onChangeText={(title) => this.setState({title})}/>
            </Item>
            <Text style={styles.des}>{strings('urlrequestinput.structure')}</Text>
            <RadioGroup
              style={styles.ratio}
              onSelect = {(index) => this.selectHandle(index)}
            >     
                <RadioButton value={'webSite'} style={styles.frm}>
                  <Text>{strings('urlrequestinput.web')}</Text>
                </RadioButton>
                      
                <RadioButton value={'facebookPage'} style={styles.frm} >
                  <Text>{strings('urlrequestinput.facepage')}</Text>
                </RadioButton>                 
            </RadioGroup>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>{strings('urlrequestinput.url')}</Label>
              <Input placeholder={strings('urlrequestinput.blururl')} style={styles.all} onChangeText={(link) => this.setState({link})}/>
            </Item>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>{strings('urlrequestinput.contact')}</Label>
              <Input placeholder={strings('urlrequestinput.blurphone')} style={styles.all} onChangeText={(phone) => this.setState({phone})}/>
            </Item>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>{strings('urlrequestinput.email')}</Label>
              <Input placeholder="Info@mail.com" style={styles.all} onChangeText={(email) => this.setState({email})}/>
            </Item>
            <Item stackedLabel style={styles.all}>
              <Label style={styles.frm}>{strings('urlrequestinput.address')}</Label>
              <Input placeholder={strings('urlrequestinput.bluraddress')} style={styles.all} onChangeText={(address) => this.setState({address})}/>
            </Item>


            <Image source={this.state.pickedImage} style={styles.previewImage} defaultSource={require('../Image/logo-KD-small.png')} />
            <TouchableOpacity style={{ backgroundColor: 'green', margin: 10, padding: 10 }}
              onPress={this.pickImageHandler}
            >
              <Text style={{color:'#fff'}}>{strings('urlrequestinput.image')}</Text>
            </TouchableOpacity>

            {/* <Image source={this.state.avatarSource} style={styles.uploadAvatar} /> */}
              <Text style={Object.assign({},styles.frm,styles.des)}>{strings('urlrequestinput.webcate')}</Text>
              <Picker
                selectedValue={this.state.cate}
                style={Object.assign({},styles.picker)}
                onValueChange={(itemValue, itemIndex) => this.setState({ cate: itemValue })}>
                <Picker.Item label="Java" value="java" />
                <Picker.Item label="JavaScript" value="js"/>
              </Picker>

              <Text style={Object.assign({},styles.frm,styles.des)}>{strings('urlrequestinput.subcate')}</Text>
              <Picker
                selectedValue={this.state.subcate}
                style={Object.assign({},styles.picker)}
                onValueChange={(itemValue, itemIndex) => this.setState({ subcate: itemValue })}>
                <Picker.Item label="Java" value="java" />
                <Picker.Item label="JavaScript" value="js"/>
              </Picker>

               <Item stackedLabel style={styles.all}>
                <Label>Keyword</Label>
                <Input placeholder="add a tag" style={styles.all} onChangeText={(keyword) => this.setState({keyword})}/>
              </Item>              
             
              <Label style={Object.assign({},styles.frm,styles.des)}>Discription</Label>
              <Textarea rowSpan={5} bordered style={Object.assign({},styles.all,styles.textArea)} onChangeText={(description) => this.setState({description})}/>

             <Button block style={styles.btn} onPress={this.funcAddUrl}>
              <Text style={{color:'white'}}>{strings('urlrequestinput.btnrequest')}</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

// export default FormInput

const mapStateToProps = (state) => {
  return {
    isCreated: state.knongdais.isCreated
  }
}

export default connect(mapStateToProps, { createUrl })(URLRequestInput);



const styles = StyleSheet.create({
  all: {
    borderColor: 'bisque',
    fontFamily: 'Roboto',
    fontSize: 13,
  },
  allColor: {
    color: '#666666',

  },
  frm: {
    fontSize: 16,
  },
  picker: {
    marginLeft: 15,
    borderColor: 'bisque'
  },
  des: {
    marginTop: 20,
    marginLeft: 15,
    fontFamily: 'Roboto',
    fontSize: 16,
  },
  textArea: {
    fontFamily: 'Roboto',
    fontSize: 13,
    flex: 1,
    marginTop: 10,
    marginBottom: 20,
    marginLeft: 15,
  },
  btn: {
    fontFamily: 'Roboto',
    fontSize: 16,
    width: 200,
    alignSelf: 'center',
    marginBottom: 10,
    backgroundColor: '#F7941D'
  },
  ratio: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 10,
  },
  previewImage: {
    width: 100,
    height: 100,
    marginTop: 10,
    marginLeft: 20,
  }
})
