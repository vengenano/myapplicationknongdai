import React, { Component } from 'react'
import { 
    View, StyleSheet, Platform, FlatList,
    Image, ScrollView, TouchableOpacity,
    ImageBackground ,
    PixelRatio
} from 'react-native'
import { Container, Header, Content, List, ListItem, Button, Text, Left, Right, Icon, Card, CardItem, Switch, Body } from 'native-base';
import SearchBar from '../ReusableComponent/SearchBar';
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import {fetchResult} from '../Actions/KnongDaiAction'

//small device iphon 5s,iphone 6...
const isSmallScreen = PixelRatio.get() <= 2
 class Result extends Component {
   
    state = {
        searchValue:'',
        searchSubVal:'',
        page:1,
        loading: false,
        results:[],
        pagination:{},
        total_record: this.props.totalRecord,
        statusModal: false,
        resultData: []
    }
    onChangeValue ( newValue ){
        this.setState({
            searchValue: newValue
        })
    }

    fetchResult = () => {
        var txt = this.props.searchValue? this.props.searchValue:this.state.searchValue
        var subCategoryId = this.props.subCategoryId   
        
        this.txtSearchBox()  
        this.props.fetchResult(txt,subCategoryId, this.state.page);
    }

    componentWillMount(){
       this.fetchResult()
    }
    componentDidUpdate(){
        var txt = this.state.searchValue
        this.props.fetchResult(txt,"",this.state.page);
    }
    txtSearchBox(){
        this.setState({
            searchValue: this.props.searchValue && this.props.searchValue,
            searchSubVal:this.props.subCategoryName && this.props.subCategoryName
        })
    }
    componentDidMount(){
        this.txtSearchBox()  
    }
    checkImageUri(imgUrl) {
        if (!imgUrl.includes("http"))
            imgUrl = ActionType.BASE_RUL + imgUrl;
           
        return imgUrl;
    }
    handleLoadMore(){ 
    //    const { page } = this.state
    //    if (page < this.props.pagination.total_page) 
            this.setState({page: this.state.page + 1}, () => this.fetchResult());
        // this.setState({
        //     page: page + 1,
        // },()=>{
        //     this.props.fetchResult(this.props.searchValue, this.props.subCategoryId, page)
        //     // var txt = this.state.searchValue
        //     // this.props.fetchResult(txt,"",this.state.page); 
        //     this.setState({
        //         loading: false,
        //     })
        // })
    }
    // handleOnResultClick(){
    //     Actions.webview()
    // }
    handleOnResultClick(id,title,link){
        const param = {
            id: id,
            title: title,
            link:link
        }
        this.setState({
            statusModal:false
        })
        Actions.webview(param)
    }


    componentWillReceiveProps(nextProps){
      if (nextProps.knongdaiResultss !== this.props.knongdaiResultss) {
        this.setState({ 
            results: this.state.searchValue? [...nextProps.knongdaiResultss]: [...this.state.results,...nextProps.knongdaiResultss] ,
          //  ...this.state,results:  {...this.state.results,...nextProps.knongdaiResultss},
            pagination: nextProps.pagination,

        })
      }
    }
    render() {
        // console.log('Search Result : ',this.props)
        // this.setState({resultData: this.props.knongdaiResult.data})
        const {resultData} = this.state
        // resultData = resultData.length ==  0 ? this.setState({resultData: this.props.knongdaiResult.data}) :  
        //     this.setState({...this.state.resultData, ...this.props.knongdaiResult.data})
        let data = this.state.resultData? this.props.knongdaiResult.data : this.state.knongdaiResult.push(this.props.knongdaiResult.data)

        return (
            <View style={styles.container}>
                {/*header contain Search Bar and back btn */}
                <View style={styles.header}>
                    <View style={styles.btnBack}>
                        <Button transparent onPress={() => Actions.pop()}>
                            <Image source={require('../Image/icons8-left-100.png')} style={styles.imgBack} />
                        </Button>
                    </View>
                    <View style={[styles.searchBox]}>
                        <SearchBar  
                        initialValue = {this.state.searchValue?this.state.searchValue:this.state.searchSubVal}
                        changeValue={this.onChangeValue.bind(this)}
                        />
                    </View>
                </View>
                {/*End header contain Search Bar and back btn */}
                <View style={styles.listBox}>
                    <View style={styles.txtBox}>
                        <Text style={styles.txtStyle}>There are&nbsp;
                        <Text style={styles.txtOrange}>
                            {/* {this.props.totalRecords} */}
                            {this.state.pagination.total_record}
                        </Text>
                            &nbsp;results for&nbsp;
                        <Text style={styles.txtOrange}>{this.state.searchValue?this.state.searchValue:this.state.searchSubVal}</Text>
                            &nbsp;keyword.
                        </Text>
                    </View> 
                    <FlatList
                    ItemSeparatorComponent={({highlighted}) => (
                        <View style={[ highlighted && {marginLeft: 0}]} />
                    )}
                    //data={data}
                    data={this.state.results}
                    keyExtractor={(item,index)=>{return ""+index}}
                    //onEndReached={()=>this.handleLoadMore()}
                    //onEndReachedThreshold={1}
                    renderItem={({item, index}) => (
                        <TouchableOpacity
                            onPress={()=>this.handleOnResultClick(item.id,item.title,item.link)}
                            style={styles.boxContainer}
                            key={item.id}
                            >
                            <Card style={styles.cardBox}>
                                <CardItem>
                                    <View style={{width:'25%',paddingRight:5,}}>
                                        <ImageBackground
                                            resizeMode={item.pic_url?"contain":"cover"}
                                            style={[styles.bigLogo,{opacity:item.pic_url ? 1:0.5,}]}
                                            source={item.pic_url ? 
                                                    { uri: this.checkImageUri(item.pic_url) } : require('../Image/logo-kd-circle-gray.png')
                                            }
                                        />
                                    </View>
                                    <Body>
                                        {
                                            item.title?
                                            <View style={styles.contentBox}>
                                                <View>
                                                    <Image 
                                                        source={item.pic_url ? 
                                                            { uri: this.checkImageUri(item.pic_url) } : require('../Image/logo-kd-circle-gray.png')
                                                        }
                                                        style={styles.smallLogo}
                                                    />
                                                </View>
                                                <View><Text style={styles.txtTitle}>{item.title}</Text></View>
                                            </View>
                                            :<View></View>
                                        }
                                        {
                                            item.phone?
                                            <View style={styles.contentBox}>
                                                <View>
                                                    <Image 
                                                        source={require('../Image/icons8-ringer-volume-filled-100.png')}
                                                        style={styles.smallLogo}
                                                    />
                                                </View>
                                                <View><Text style={styles.txtcontent}>{item.phone}</Text></View>
                                            </View>
                                            :<View></View>
                                        }
                                        {
                                            item.link?
                                            <View style={styles.contentBox}>
                                                <View>
                                                    <Image 
                                                        source={require('../Image/icons8-globe-filled-100.png')}
                                                        style={styles.smallLogo}
                                                    />
                                                </View>
                                                <View><Text style={styles.txtLink}>{item.link}</Text></View>
                                            </View>
                                            :<View></View>
                                        }
                                        {
                                           item.email? 
                                            <View style={styles.contentBox}>
                                                <View>
                                                    <Image 
                                                        source={require('../Image/icons8-gmail-filled-100.png')}
                                                        style={styles.smallLogo}
                                                    />
                                                </View>
                                                <View><Text style={styles.txtcontent}>{item.email}</Text></View>
                                            </View>
                                            :<View></View>
                                        }
                                        {
                                           item.address? 
                                            <View style={styles.contentBox}>
                                                <View>
                                                    <Image 
                                                        source={require('../Image/icons8-place-marker-100.png')}
                                                        style={styles.smallLogo}
                                                    />
                                                </View>
                                                <View><Text style={styles.txtaddress}>{item.address}</Text></View>
                                            </View>
                                            :<View></View>
                                        }                                            
                                    </Body>
                                </CardItem>
                            </Card>
                        </TouchableOpacity>  
                    )}
                    />                   
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        knongdaiResult: state.knongdais.knongdaiResult,
        knongdaiResultss: state.knongdais.knongdaiResult.data,
        pagination: state.knongdais.knongdaiResult.pagination, 
    }
    
}
 export default connect(mapStateToProps, {fetchResult})(Result);


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingTop: Platform.OS === 'ios' ? isSmallScreen? 20: 40 : 0,
        flex: 1
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
    },
    searchBox: {
        width: '88%',
        alignSelf: 'flex-end',
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        paddingTop: 5,
        paddingBottom: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
    },
    btnBack: {
        alignSelf: 'flex-start',marginTop:10,
    },
    imgBack: { width: 25, height: 25, marginLeft: 10, marginRight: 5 },
    txtStyle: { margin: 10, textAlign: 'center', color: '#666666', fontSize: 13, },
    txtBox: {
        borderBottomColor: '#eee', borderBottomWidth: 1, width: '100%',
        marginBottom: -10,
    },
    txtOrange: { color: '#F7941D', fontSize: 13 },
    listBox: {
        flex: 1, marginTop: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingBottom:Platform.OS==='ios'?45:30,
    },
    contentBox: {
        flexDirection: 'row', marginTop: 3,
        paddingRight: 15,
    },

    smallLogo: { width: 18, height: 18, marginRight: 5 },
    bigLogo: {
        width:80,height:80
    },
    txtTitle: { fontWeight: 'bold', fontSize: 14, color: '#444444' },
    txtcontent: { color: '#444444', fontSize: 13, },
    txtaddress: { color: '#444444', fontSize: 12, },
    txtLink: { color: '#179CB4', fontSize: 12, },
    boxContainer: {
        width: '100%',paddingBottom:3,
    },
    cardBox:{paddingBottom:8,paddingTop:2}
})
