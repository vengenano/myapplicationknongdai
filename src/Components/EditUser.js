import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TextInput, AsyncStorage } from "react-native";
import {
  Container, Content, Form, Item, Label, Input, Card, CardItem,
  Icon, Right, Thumbnail, Button, Header, Left
} from "native-base";
import ImageLogo from './imageLogo'
import { strings, chanageLanguage } from '../Language/ActionSwitch/i18n.js'
import I18n from 'react-native-i18n'
import { Actions } from "react-native-router-flux";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import { updateUser } from '../Actions/KnongDaiAction';
import { connect } from 'react-redux';
class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: I18n.locale,
      gender: '',
      userName: ''
    };
  }
  selectHandle(index, value) {
    if (index == 0) {
      this.setState({
        gender: value
      }),
        console.log('index', index, "Value", value)
    } else if (index == 1) {
      this.setState({
        gender: value
      })
    }
  }
  componentWillUpdate() {
    AsyncStorage.getItem("language")
      .then(lang => {
        chanageLanguage(lang)
        this.setState({ language: lang })
      })
  }

  handleOnSaveButton = async () => {
    try {
      const facebookId = await AsyncStorage.getItem('facebookId');
      this.props.updateUser(facebookId, this.state.userName, this.state.gender)
      await AsyncStorage.setItem('userName', this.state.userName);
      await AsyncStorage.setItem('gender', this.state.gender);
      Actions.userprofile({ type: "reset" })
      Actions.drawer({ type: "reset" })
      Actions.home({ type: "reset" })   
      Actions.home()
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  render() {
    return (
      <Container>
        <Content>
          <ImageLogo />
          <Card style={{marginHorizontal:10}}>
            <Form style={styles.frm}>
              <Item stackedLabel style={styles.all}>
                <Label style={Object.assign({}, styles.frm, styles.label)}>
                  {strings('user-edit.username')}
                </Label>
                <Input
                  placeholder={strings('user-edit.blurname')}
                  defaultValue={this.props.userName}
                  onChangeText={(userName) => this.setState({ userName })}
                  style={Object.assign({}, styles.all, styles.input)}
                />
              </Item>
              <Text style={styles.gen}>{strings('user-edit.gender')}</Text>

              <RadioGroup style={styles.ratio}
                selectedIndex={this.props.gender == 'F' ? 0 : 1}
                color="#F7941D"
                onSelect={(index, value) => this.selectHandle(index, value)}
              >
                <RadioButton value={"F"} style={styles.frm}>
                  <Text>{strings('user-edit.female')}</Text>
                </RadioButton>
                <RadioButton value={"M"} style={styles.frm}>
                  <Text>{strings('user-edit.male')}</Text>
                </RadioButton>
              </RadioGroup>

              <Item stackedLabel style={styles.all}>
                <Label style={Object.assign({}, styles.frm, styles.label)}>
                  {strings('user-edit.phone')}
                </Label>
                <Input
                  caretHidden
                  placeholder={strings('user-edit.blurphone')}
                  value={this.props.phoneNumber}
                  style={Object.assign({}, styles.all, styles.input)}
                />
              </Item>
              <Item style={styles.item}>
                <Left>
                  <Button
                    onPress={() => Actions.pop()}
                    block style={styles.btn}>
                    <Text style={styles.btnTxt}>{strings('user-edit.btncancel')}</Text>
                  </Button>
                </Left>
                <Right>
                  <Button block style={styles.btn}
                    onPress={() => this.handleOnSaveButton()}
                  >
                    <Text style={styles.btnTxt}>{strings('user-edit.btnsave')}</Text>
                  </Button>
                </Right>
              </Item>
            </Form>
          </Card>
        </Content>
      </Container>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isUserUpdate: state.knongdais.isUserUpdate,
  }
}
export default connect(mapStateToProps, { updateUser })(EditUser)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    // justifyContent: 'center',
    fontFamily: "Roboto"
  },
  title: {
    fontSize: 32,
    color: "#F7941D",
    marginBottom: 10
  },
  card: {
    marginTop: 20,
    borderColor: "#F7941D",
    borderWidth: 10
  },
  rigth: {
    marginRight: -60
  },
  itemtitle: {
    marginLeft: 15,
    fontSize: 16,
    marginTop: 10
  },
  item: {
    fontSize: 18,
    marginLeft: 30,
    marginTop: 5
  },
  buttom: {
    marginBottom: 10
  },
  cardPhoto: {
    marginTop: 10,
    borderColor: "#F7941D",
    borderWidth: 10,
    marginLeft: 20,
    marginRight: 20,
    height: 150,
    alignItems: "center",
    justifyContent: "center"
  },
  ratio: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 20
  },
  frm: {
    fontSize: 16,
  },
  btn: {
    fontFamily: "Roboto",
    fontSize: 16,
    width: 100,
    alignSelf: "center",
    marginBottom: 10,
    marginTop: 10,
    backgroundColor: '#F7941D',
    borderRadius:25,
  },
  all: {
    borderColor: "bisque",
    fontFamily: "Roboto",
    fontSize: 13
  },
  gen: {
    marginTop: 20,
    marginLeft: 10,
    fontFamily: "Roboto",
    fontSize: 16,
    fontWeight:'bold'
  },
  label: {
    // marginLeft: 10
    fontWeight:'bold'
  },
  input: {
    // marginRight: -10
  },
  btnTxt:{ 
    color: 'white' ,
    fontWeight: 'bold',
  }
});
