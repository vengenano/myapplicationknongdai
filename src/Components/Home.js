import React, { Component } from 'react';
import {
    View, Text, StyleSheet, Platform, Image, Dimensions, TouchableOpacity,
    TextInput, ScrollView,
    TouchableHighlight, FlatList,
    Animated,
    AsyncStorage,
    PixelRatio,
} from 'react-native'
import {strings,chanageLanguage} from '../Language/ActionSwitch/i18n.js'
import I18n from 'react-native-i18n'
import { Container, Button } from 'native-base';
import { connect } from 'react-redux';
import { ActionType } from "../Actions/ActionType"
import { fetchMainCategory, fetchSubCategoryByMainId } from '../Actions/KnongDaiAction';
import SearchBar from '../ReusableComponent/SearchBar';
import { Actions } from 'react-native-router-flux'
import Modal from 'react-native-modal'
import OneSignal from 'react-native-onesignal'; // Import package from node modules of OneSignal

var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;
var android = Platform.OS === 'android';
var ios = Platform.OS === 'ios';
var isSmallDevice = PixelRatio.get() <= 2
class Home extends Component {
    constructor() {
        super()
        this.state = {
            statusModal: false,
            searchValue: '',
            main_cat_name: '',
            main_icon_url: '',
            userPlayerId:'',
            reloadFake: 0,
            language: I18n.locale
        }

       
    }

    componentWillUpdate() {
        AsyncStorage.getItem("language")
        .then(lang => {
            chanageLanguage(lang)
            this.setState({language: lang})
        })
    }
    
    onChangeValue(newValue) {
        this.setState({
            searchValue: newValue
        })
    }
    getSubCategoryByMainId(id, main_cat_name, main_icon_url) {
        if (android) {      
            this.props.fetchSubCategoryByMainId(id) 
            this.setState({
                main_cat_name: main_cat_name,
                main_icon_url: main_icon_url,
                statusModal: true,
            })
        }
        else {
            const param = {
                id: id,
                main_cat_name: main_cat_name,
                main_icon_url: main_icon_url
            }
            Actions.modal(param)
        }


    }
    handleOnSubCategoryClick(subCategoryId, subCategoryName, totalRecord) {
        const param = {
            subCategoryId: subCategoryId,
            subCategoryName: subCategoryName,
            totalRecord: totalRecord
        }
        this.setState({
            statusModal: false
        })
        Actions.result(param)
    }
    checkImageUri(imgUrl) {
        if (!imgUrl.includes("http"))
            imgUrl = ActionType.BASE_RUL + imgUrl;

        return imgUrl;
    }
    GetSectionListItem = (item) => {
    }
    getUser = async () => {
        try {
            const facebookId = await AsyncStorage.getItem('facebookId');
            const phoneNumber = await AsyncStorage.getItem('phoneNumber');
            const userName = await AsyncStorage.getItem('userName');
            const gender = await AsyncStorage.getItem('gender');
            this.setState({ facebookId, phoneNumber, userName, gender });
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }
    componentWillMount() {
        this.props.fetchMainCategory();       
    }   
    render() {
        const { knongdai } = this.props
        return (
            <View style={styles.container}>
                {/* start Modal */}
                <Modal
                    animationInTiming={500}
                    animationOutTiming={300}
                    animationIn={'zoomInDown'}
                    animationOut={'zoomOutDown'}
                    onSwipe={() => this.setState({ statusModal: false })}
                    onBackdropPress={() => this.setState({ statusModal: false })}
                    onBackButtonPress={() => this.setState({ statusModal: false })}
                    swipeDirection={Platform.OS === 'android' ? "right" : "up"}
                    backdropColor='rgba(0,0,0,0)'
                    backdropOpacity={0.5}
                    onRequestClose={() => this.setState({ statusModal: false })}
                    isVisible={this.state.statusModal}
                    onSwipeThreshold={20}
                    avoidKeyboard={true}
                >
                    <View style={styles.containerModalSub}>
                        <View style={styles.bntCloseSub}>
                            {/* Handle on close button */}
                            <View>
                                <TouchableOpacity
                                    style={styles.closeSub}
                                    onPress={() => this.setState({ statusModal: false })}
                                >
                                    <Image
                                        source={require('../Image/icons8-delete-100-white.png')}
                                        style={{ width: 25, height: 25 }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.headerBoxSub}>
                            {/* Handle on title and image that get from click */}
                            <View style={styles.circleBox1Sub}>
                                <Image
                                    style={{ width: 60, height: 60 }}
                                    source={{ uri: ActionType.BASE_RUL + this.state.main_icon_url }}
                                />
                            </View>
                            <View style={styles.txtBoxSub}>
                                <Text style={styles.txtTitleSub}>{this.state.main_cat_name}</Text>
                            </View>
                        </View>
                        <View style={styles.listBoxSub}>
                            <FlatList
                                ItemSeparatorComponent={({ highlighted }) => (
                                    <View style={[highlighted && { marginLeft: 0 }]} />
                                )}
                                data={this.props.knongdaiSubCategory}
                                keyExtractor={(item, index) => { return "" + index }}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity
                                        onPress={() => this.handleOnSubCategoryClick(item.id, item.cate_name, item.total_url)}
                                        key={index}
                                        style={styles.listStyleSub}>
                                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
                                            <View style={styles.circleBoxSub}>
                                                <Image
                                                    source={require('../Image/icons8-login-100.png')}
                                                    style={{ width: 20, height: 20 }}
                                                />
                                            </View>
                                            <View style={{ alignSelf: 'center', flex: 5 }}>
                                                <Text style={styles.txtStyleSub}>{item.cate_name}</Text>
                                            </View>
                                            <View style={styles.totalBoxSub}>
                                                <Text style={styles.txtTotalSub}>{item.total_url}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    </View>

                </Modal>
                {/* end modal */}
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                {/* Logo */}
                <View style={{ marginTop: 15 ,alignSelf:'center'}}>
                    <Image source={require('../Image/logo-KD-small.png')}
                        style={{ width: 100, height: 100 }}
                    />
                </View>
                {/* Search */}
                <View style={{ zIndex: 10000 }}>
                    <SearchBar
                        propHome={"home"}
                        initialValue={this.state.searchValue}
                        changeValue={this.onChangeValue.bind(this)}
                    />
                </View>
                {/* end Search */}

                {/* category */}
               
                    <View style={[styles.boxWrapper, styles.shadow]}>
                        {knongdai.map((knongdaiObj, index) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => this.getSubCategoryByMainId(knongdaiObj.id, knongdaiObj.cate_name, knongdaiObj.icon_name)}
                                    style={[styles.fifty, styles.box]} key={knongdaiObj.id}>
                                    <View>
                                        <View style={styles.circleBox}>
                                            <Image
                                                source={knongdaiObj.icon_name ? { uri: this.checkImageUri(knongdaiObj.icon_name) } : require('../Image/icons8-expand-arrow-100.png')}
                                                style={styles.imgCircle} />
                                        </View>
                                        {/* <View style={styles.totalBox}>
                                            <Text style={styles.txtTotal}>{knongdaiObj.total_url}</Text>
                                        </View> */}
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={styles.txtCenter}>{knongdaiObj.cate_name}
                                            <Text style={styles.txtTotal1}>  ( {knongdaiObj.total_url} )</Text>
                                        </Text>
                                    </View>
                                </TouchableOpacity>

                            )
                        })}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        knongdai: state.knongdais.knongdai,
        knongdaiSubCategory: state.knongdais.knongdaiSubCategory
    }
}
export default connect(mapStateToProps, { fetchMainCategory, fetchSubCategoryByMainId })(Home);


const styles = StyleSheet.create({
    menuBar: {
        backgroundColor: '#F7941D',
        padding: 15,
        paddingTop: Platform.OS === 'ios' ? 38 : 5,
        paddingBottom: 5,
        width: '100%',
        borderRadius: 0,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    fifty: {
        width: '48%'
    },
    boxWrapper: {
        flex: 1, margin: 5, marginTop: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingBottom: Platform.OS==='ios'? isSmallDevice? 0: 20:0,
        alignItems: 'center',
        justifyContent:'center'
    },
    box: {
        backgroundColor: "rgba(62, 127, 131,0.1)",
        justifyContent: 'center',
        alignItems: 'center',
        height: (width/2),
        margin: '1%',
        borderColor: 'rgba(0,0,0,0.06)',
        borderRadius: 5,
        borderWidth: 4,
    },
    btnDown: {
        width: 18, height: 18, borderColor: '#F7941D', borderWidth: 1, borderRadius: 9
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.2,
        shadowRadius: 5,
    },

    modalBox: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 50
    },
    circleBox: {
        backgroundColor: 'white', 
        marginTop: 25, 
        padding: 15, 
        borderRadius: 65
    },
    imgCircle:{ 
        width: isSmallDevice? 45: 65, 
        height:isSmallDevice? 45: 65, 
    },
    circleBox1: {
        backgroundColor: 'white', padding: 15, borderRadius: 65
    },
    bntClose: {
        flexDirection: 'row',
        backgroundColor: 'rgba(234, 151, 62,1)',
        width: '100%',
        justifyContent: 'flex-end',
    },
    close: { marginTop: 10, marginRight: 15 },
    headerBox: {
        flexDirection: 'row',
        padding: 20, width: '100%',
        justifyContent: 'center',
        backgroundColor: 'rgba(234, 151, 62,1)',
    },
    txtBox: { marginTop: 20, marginLeft: 10 },
    txtTitle: { fontSize: 30, color: 'white' },
    listBox: {
        flex: 1, marginTop: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingBottom: 150,
        backgroundColor: 'rgba(234, 151, 62,0.8)'
    },
    listStyle: { width: '100%', padding: 20, borderTopColor: '#eee', borderTopWidth: 0.5, },
    txtStyle: { marginLeft: 20, fontSize: 20, color: 'white' },
    txtCenter: { 
        paddingLeft: 20, 
        paddingTop: isSmallDevice? 5: 10, 
        textAlign: 'center' ,
        fontSize: isSmallDevice? 11:13,
        fontWeight:'bold'
    },
    totalBox: { alignSelf: 'center', alignSelf: 'flex-end', marginTop: -15, backgroundColor: 'orange', borderRadius: 25, width: 25, height: 25 },
    txtTotal: { 
        color: 'white', 
        fontSize: 10, 
        textAlign: 'center', 
        marginTop: 5, 
        fontWeight: 'bold' ,
        textShadowColor: 'rgba(0, 0, 0, 0.3)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    },

    // state modal style
    circleBoxSub: {
        backgroundColor: 'white', padding: 5, borderRadius: 65
    },
    circleBox1Sub: {
        backgroundColor: 'white', padding: 10, borderRadius: 65,
        alignSelf: 'center'
    },

    containerModalSub: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingBottom: 0,
        borderRadius: 10,
        borderColor: '#eee',
        borderWidth: 1,
        width: '90%',
        alignSelf: 'center'
    },
    bntCloseSub: {
        flexDirection: 'row',
        backgroundColor: 'rgba(247, 148, 29,1)',
        width: '100%',
        justifyContent: 'flex-end',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,

    },
    closeSub: {
        marginTop: 10,
        marginRight: 15

    },
    headerBoxSub: {
        paddingRight: 10,
        paddingLeft: 10,
        width: '100%',
        justifyContent: 'center',
        backgroundColor: 'rgb(247, 148, 29)' //'rgba(234, 151, 62,1)',
    },
    txtBoxSub: { marginTop: 5, paddingBottom: Platform.OS === 'android' ? 10 : 5, },
    txtTitleSub: { 
        fontSize: 22, 
        fontWeight: 'bold', 
        color: 'white', 
        textAlign: 'center', 
        fontWeight: 'bold' ,
        textShadowColor: 'rgba(0, 0, 0, 0.3)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    },
    listBoxSub: {
        marginTop: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingBottom: 20,
        backgroundColor: 'white',
        height: height * .4,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    listStyleSub:
    {
        width: '100%',
        padding: 10,
        borderTopColor: '#eee',
        borderTopWidth: 0.5,
        zIndex: 20
    },
    txtStyleSub: { marginLeft: 20, fontSize: 14, color: '#444444' },
    txtCenterSub: { padding: 10, paddingLeft: 20, paddingRight: 20, paddingTop: 20, textAlign: 'center' },
    totalBoxSub: { alignSelf: 'center', marginTop: -5, backgroundColor: 'orange', borderRadius: 25, width: 25, height: 25 },
    txtTotalSub: { 
        color: 'white', 
        fontSize: 10, 
        textAlign: 'center', 
        marginTop: 5, 
        fontWeight: 'bold' ,
        fontWeight: 'bold' ,
        textShadowColor: 'rgba(0, 0, 0, 0.3)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    }, 
    txtTotal1: { 
        color: '#F7941D', 
        fontSize: 10,
        marginTop:4, 
        textAlign: 'center', 
        fontWeight: 'bold' ,
        paddingTop: isSmallDevice? 5: 10, 
    },

    //end modal style
})
