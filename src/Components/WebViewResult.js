import React, { Component } from 'react';
import { View, Text,Platform, WebView,Image, NetInfo, StyleSheet, ActivityIndicator } from 'react-native';
import MyWebView from 'react-native-webview-autoheight'
import {Header,CardItem,Container,Left,Button,Body,Right} from 'native-base'
import {Actions} from 'react-native-router-flux'

export default class WebViewResult extends Component {
  ActivityIndicatorLoadingView() {
    //making a view to show to while loading the webpage
    return (
      <ActivityIndicator
        color="#009688"
        size="large"
        style={styles.ActivityIndicatorStyle}
      />
    );
  }
  render() {
      return (
        Platform.OS==='android'?
        <Container>
          <Header 
            noShadow={false}
            transparent
            style={{backgroundColor:'white'}}>
            <Left style={{}}>
              <Button transparent onPress={() => Actions.pop()}>
                <Image source={require('../Image/icons8-delete-100.png')}
                  style={{ width: 25, height: 25, marginLeft: 0}} />
              </Button>
            </Left>
            <Body></Body>
            <Right></Right>
          </Header>
        <MyWebView
          source={{ uri: this.props.link }}
          startInLoadingState={true}
        />
        </Container>
        :
        <Container>
          
          <Header 
            noShadow={false}
            transparent
            style={{backgroundColor:'white'}}>
            <Left style={{}}>
              <Button transparent onPress={() => Actions.pop()}>
                <Image source={require('../Image/icons8-delete-100.png')}
                  style={{ width: 25, height: 25, marginLeft: 0}} />
              </Button>
            </Left>
            <Body></Body>
            <Right></Right>
          </Header>
          
          <WebView
            style={styles.container}
            //Loading URL
            source={{ uri: this.props.link }}
            //Enable Javascript support
            javaScriptEnabled={false}
            //For the Cache
            domStorageEnabled={true}
            //View to show while loading the webpage
            renderLoading={this.ActivityIndicatorLoadingView}
            //Want to show the view or not
            startInLoadingState={true}
          />

        </Container>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    //marginTop:Platform.OS==='ios' ?20 :0,
    marginTop:-5,
  },
  video: {
    marginTop: 20,
    maxHeight: 200,
    width: 320,
    flex: 1
  },
  ActivityIndicatorStyle:{
    flex:1,
    justifyContent:'center',
    alignSelf: 'center',
  }
});
