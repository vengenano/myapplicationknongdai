import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch, Button, Title, } from 'native-base';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { StyleSheet, Image, AsyncStorage } from 'react-native';
import I18n from 'react-native-i18n';
import { strings, chanageLanguage } from '../Language/ActionSwitch/i18n.js'
import { Actions } from 'react-native-router-flux';


export default class Setting extends Component {
    state = {
        language: I18n.locale
    }
    
    _changeLanguage = (lang) => {
        AsyncStorage.setItem("language", lang)
        chanageLanguage(lang)
        this.setState({ language: lang })
    }
    render() {


        return (
            <Container>

                <Content>
                    <List>
                        <ListItem>
                            <Body>
                                <Text style={{ color: 'black', fontSize: 18 }}>{strings('setting.language')}</Text>
                                <Text note style={{ fontSize: 11 }}>
                                    {strings('setting.defualt')}
                                </Text>
                            </Body>

                            <Right >
                                <RadioGroup
                                    style={styles.ratio}
                                    onSelect={(index, value) => this._changeLanguage(value)}
                                >
                                    <RadioButton value='km'>
                                        <Image source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Flag_of_Cambodia.svg/510px-Flag_of_Cambodia.svg.png' }} style={{ width: 30, height: 20 }} />
                                    </RadioButton>

                                    <RadioButton value='en'>
                                        <Image source={{ uri: 'https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/510px-Flag_of_the_United_Kingdom.svg.png' }} style={{ width: 30, height: 20 }} />
                                    </RadioButton>
                                </RadioGroup>
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Body >
                                <Text style={{ color: 'black', fontSize: 18 }}>{strings('setting.notification')}</Text>
                                <Text note style={{ fontSize: 11, }}>
                                    {strings('setting.allownotification')}
                                </Text>
                            </Body>
                            <Right>
                                <Switch value={true} trackColor={{ true: '#F7941D', false: 'null' }} />
                            </Right>
                        </ListItem>
                        <ListItem
                            onPress={() => Actions.policy()} >
                            <Body>
                                <Text>{strings('setting.policy')}</Text>
                            </Body>
                            <Right>
                                <Icon active name="arrow-forward" />
                            </Right>
                        </ListItem>
                        <ListItem
                            onPress={() => Actions.about()}
                        >
                            <Body >
                                <Text>{strings('setting.aboutus')}</Text>
                            </Body>
                            <Right>
                                <Icon active name="arrow-forward" />
                            </Right>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}
const styles = StyleSheet.create({
    ratio: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 20,
        // activeColor:'#F7941D', thickness:1,color:'#F791D'


    },
    itemHeight: {
        paddingTop: 10,
        paddingBottom: 10,
    }

})