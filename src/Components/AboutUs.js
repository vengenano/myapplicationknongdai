import React, { Component } from 'react';
import { Container,Header, Content, Card, CardItem, Body, Text ,Left,
Title,Right,Button
} from 'native-base';
import {StyleSheet} from 'react-native';
import ImageLogo from './imageLogo.js'
import I18n from 'react-native-i18n'
import {strings,chanageLanguage} from '../Language/ActionSwitch/i18n.js'


export default class AboutUS extends Component {
  state={
    language: I18n.locale
  }
  componentWillUpdate() {
    AsyncStorage.getItem("language")
    .then(lang => {
        chanageLanguage(lang)
        this.setState({language: lang})
    })
}

  render() {
    return (
      <Container style={{backgroundColor:'#F2F2F0'}}>
        <Content>
          {/* <ImageLogo /> */}
         <ImageLogo />

          <Text style={styles.myCardTitle}>{strings('aboutus.into')}</Text>
          <Card >
            <CardItem>
               
              <Body>
                <Text style={styles.introductionCard}>
                {strings('aboutus.introduction')}
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Text style={styles.myCardTitle}>{strings('aboutus.contact')}</Text>
          <Card>
            <CardItem>
               
              <Body>
                <Text style={styles.myStyle}>
                      <Text style={{ fontWeight: 'bold',fontSize:15,color:'#555555' }}>{strings('aboutus.address')}:    </Text>
                      <Text style={{marginLeft:20,fontSize:15,color:'#555555'}}>
                      {strings('aboutus.addressdetail')}
                      </Text>
                </Text>

                <Text style={styles.myStyle} style={{marginTop:5, fontSize:15,color:'#555555'}}>
                      <Text style={{ fontWeight: 'bold',marginLeft:10, fontSize:15,color:'#555555' }}>{strings('aboutus.tel')}:   </Text>
                      {strings('aboutus.con1')}
                </Text>
                <Text style={styles.spaceBetween}> {strings('aboutus.con2')}</Text>
                <Text style={styles.spaceBetween}> {strings('aboutus.con3')}</Text>
                <Text style={styles.spaceBetween}> {strings('aboutus.con4')}</Text>
               
              </Body>
            </CardItem>
          </Card>
 
        </Content>

        
      </Container>
    );
  }
}
const styles=StyleSheet.create({
  myStyle:{
      // fontWeight: 'bold'
      fontSize:15,
      color:'#555555'

  },
  
  spaceBetween:{
      marginLeft:35,
      fontSize:15,
      color:'#555555'
  },

  myCardTitle:{
    color:'#3E7F83',
    fontSize:19,
    fontWeight: 'bold',
    marginTop:15,
    marginLeft:10

  },
 
  introductionCard:{
    fontSize:15,
    color:'#555555'

  },

})