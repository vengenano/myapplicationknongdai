import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, AsyncStorage } from 'react-native';
import { Container, Tabs, Tab, Card, CardItem, Icon, Right, Thumbnail, Button, Header, Content } from 'native-base'
import ImageLogo from './imageLogo';
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import { fetchUserByFacebookId } from '../Actions/KnongDaiAction';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { strings, chanageLanguage } from '../Language/ActionSwitch/i18n.js'
import I18n from 'react-native-i18n'


class UserProfile extends Component {
    //_isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            facebookId: "",
            language: I18n.locale,
            gender: ''
        };
    }
    componentWillUpdate() {
        AsyncStorage.getItem("language")
            .then(lang => {
                chanageLanguage(lang)
                this.setState({ language: lang })
            })
    }

    getUser = async () => {
        try {
            const id = await AsyncStorage.getItem('facebookId');
            const gender = await AsyncStorage.getItem('gender');
            this.setState({
                facebookId: id,
                gender: gender
            })
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }
    handleOnEditUser = async () => {
        try {
            const facebookId = await AsyncStorage.getItem('facebookId');
            const phoneNumber = await AsyncStorage.getItem('phoneNumber');
            const userName = await AsyncStorage.getItem('userName');
            const gender = await AsyncStorage.getItem('gender');
            const param = {
                facebookId: facebookId,
                userName: userName,
                gender: gender,
                phoneNumber: phoneNumber
            }
            Actions.edituser(param)
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }
    componentWillMount() {
        this.getUser()
    }
    componentDidMount = () => {
        const facebookId = "";
        AsyncStorage.getItem('facebookId')
            .then(res => this.props.fetchUserByFacebookId(res))
    }
    render() {
        const { knongdaiUser } = this.props
        return (
            <Container>
                <Content>
                    <Tabs
                        tabStyle={{ borderBottomWidth: 2, borderBottomColor: 'rgb(62, 127, 131)' }}
                    >
                        <Tab heading={strings('userprofile.personal')}
                            tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#F7941D' }}
                            activeTabStyle={{ backgroundColor: 'whitesmoke' }}
                            activeTextStyle={{ color: 'rgb(62, 127, 131)', fontWeight: 'bold' }}
                        >
                            <Card style={Object.assign({}, styles.card, styles.title)}>
                                <ImageLogo />
                                <Text style={styles.itemtitle}>{strings('userprofile.name')}</Text>
                                <Text style={styles.item}>{knongdaiUser.userName}</Text>
                                <Text style={styles.gen}>{strings('userprofile.gender')}</Text>
                                <Text style={styles.item}>{this.state.gender}</Text>
                                <Text style={styles.itemtitle}>{strings('userprofile.phone')}</Text>
                                <Text style={Object.assign({}, styles.item, styles.buttom)}>{knongdaiUser.phoneNumber}</Text>

                            </Card>
                            <Button
                                style={styles.btnBox}
                                onPress={() => this.handleOnEditUser()}
                            >
                                <Text style={styles.btnTxt}>{strings('userprofile.btnedit')}</Text>
                            </Button>
                        </Tab>
                        <Tab heading={strings('userprofile.history')}
                            tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#F7941D' }}
                            activeTabStyle={{ backgroundColor: 'whitesmoke' }}
                            activeTextStyle={{ color: 'rgb(62, 127, 131)', fontWeight: 'bold' }}
                        >
                            <Card style={styles.card}>
                                <CardItem>
                                    <Text style={{ color: '#3E7F83' }}>School</Text>
                                    <Right style={styles.rigth}>
                                        <Icon name="paper-plane" style={{ color: '#3E7F83' }} />
                                    </Right>
                                </CardItem>
                                <CardItem>
                                    <Text style={{ color: '#3E7F83' }}>Hospital</Text>
                                    <Right style={styles.rigth}>
                                        <Icon name="paper-plane" style={{ color: '#3E7F83' }} />
                                    </Right>
                                </CardItem>
                            </Card>
                        </Tab>
                    </Tabs>
                </Content>
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        knongdaiUser: state.knongdais.knongdaiUser,
    }
}
export default connect(mapStateToProps, { fetchUserByFacebookId })(UserProfile)
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Roboto'
    },
    title: {
        fontSize: 32,
        color: '#F7941D',
        marginBottom: 10,
    },
    card: {
        marginTop: 5,
        borderWidth: 10,
    },
    rigth: {
        marginRight: -60,
    },
    itemtitle: {
        marginLeft: 15,
        fontSize: 16,
        marginTop: 10,
    },
    item: {
        fontSize: 18,
        marginLeft: 30,
        marginTop: 5,
    },
    buttom: {
        marginBottom: 10,
    },
    cardPhoto: {
        marginTop: 10,
        borderColor: '#F7941D',
        borderWidth: 10,
        marginLeft: 20,
        marginRight: 20,
        height: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },
    ratio: {
        flex: 1,
        flexDirection: "row",
        marginLeft: 20
    },
    frm: {
        fontSize: 16
    },
    gen: {
        marginTop: 20,
        marginLeft: 10,
        fontFamily: "Roboto",
        fontSize: 16
    },
    btnBox:
    {
        alignSelf: 'center',
        width: 200,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F7941D',
        borderRadius: 25,
        marginVertical: 10,
    },
    btnTxt: {
        alignSelf: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold',
    }
})
