import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, Modal, Image, PixelRatio,Dimensions, TouchableOpacity, TextInput, ScrollView } from 'react-native'
import { fetchSubCategoryByMainId } from '../Actions/KnongDaiAction';
import { connect } from 'react-redux';
import {ActionType} from "../Actions/ActionType"
import { Actions } from 'react-native-router-flux'

//small device iphon 5s,iphone 6...
const isSmallScreen = PixelRatio.get() <= 2
class ModalScreen extends Component {
    constructor() {
        super()
        this.state = {
            statusModal: false,
            main_cat_name: '',
            main_icon_url: ''
        }
    }
    componentWillMount() {
        this.props.fetchSubCategoryByMainId(this.props.id)
    }
    handleOnSubCategoryClick(subCategoryId,subCategoryName){
        const param = {
            subCategoryId: subCategoryId,
            subCategoryName: subCategoryName
        }
        Actions.result(param)
    }
    render() {
        const { knongdaiSubCategory } = this.props
        return (
            <View style={styles.containerModal}>
                <View style={styles.bntClose}>
                    {/* Handle on close button */}
                    <View>
                        <TouchableOpacity
                            style={styles.close}
                            onPress={() => Actions.pop()}
                        >
                            <Image
                                source={require('../Image/icons8-delete-100-white.png')}
                                style={{ width: 25, height: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.headerBox}>
                    {/* Handle on title and image that get from click */}
                    <View style={styles.circleBox1}>
                        <Image
                            style={{ width: 80, height: 80 }}
                            // source={{ uri: 'http://localhost:9090' + this.props.main_icon_url }}
                            source={{ uri: ActionType.BASE_RUL + this.props.main_icon_url }}
                        />
                    </View>
                    <View style={styles.txtBox}>
                        <Text style={styles.txtTitle}>{this.props.main_cat_name}</Text>
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View
                        style={styles.listBox}>
                        {knongdaiSubCategory.map((item, index) => {
                            return (    
                                <TouchableOpacity
                                    // onPress={()=>console.log('Sub Category is Click have ID :',item.id)}
                                    onPress={()=>this.handleOnSubCategoryClick(item.id,item.cate_name)}
                                    key={index}
                                    style={styles.listStyle}>
                                    <View style={{ flexDirection: 'row',flexWrap:'wrap', }}>
                                        <View style={styles.circleBoxSub}>
                                            <Image
                                                source={require('../Image/icons8-login-100.png')}
                                                style={{ width: 20, height: 20 }}
                                            />
                                        </View>
                                        <View style={{ alignSelf: 'center',flex:5 }}>
                                            <Text style={styles.txtStyle}>{item.cate_name}</Text>
                                        </View>
                                        <View style={styles.totalBox}>
                                            <Text style={styles.txtTotal}>{item.total_url}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        knongdaiSubCategory: state.knongdais.knongdaiSubCategory
    }
}
export default connect(mapStateToProps, { fetchSubCategoryByMainId })(ModalScreen);
const styles = StyleSheet.create({
    menuBar: {
        backgroundColor: '#F7941D',
        padding: 15,
        paddingTop: Platform.OS === 'ios' ? 38 : 5,
        paddingBottom: 5,
        width: '100%',
        borderRadius: 0,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    fifty: {
        width: '48%'
    },
    boxWrapper: {
        flex: 1, margin: 5, marginTop: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingBottom: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },
    box: {
        backgroundColor: "rgba(62, 127, 131,0.05)",
        justifyContent: 'center',
        alignItems: 'center',
        height: 180,
        margin: '1%',
        borderColor: '#eee',
        borderRadius: 5,
        borderWidth: Platform.OS === 'android' ? 1 : 0,
    },
    btnDown: {
        width: 18, height: 18, borderColor: '#F7941D', borderWidth: 1, borderRadius: 9
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.2,
        shadowRadius: 5,
    },

    modalBox: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 50
    },
    circleBox: {
        backgroundColor: 'white', marginTop: 45, padding: 15, borderRadius: 65
    },
    circleBox1: {
        backgroundColor: 'white', padding: 15, borderRadius: 65,
        alignSelf: 'center'
    },
    circleBoxSub: {
        backgroundColor: 'white', padding: 5, borderRadius: 65
    },
    containerModal: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingBottom: 50,
    },
    bntClose: { 
        flexDirection: 'row', 
        backgroundColor: 'rgba(247, 148, 29,1)',
        width: '100%', 
        justifyContent: 'flex-end'
    },
    close: { 
        marginTop: Platform.OS === 'ios' ? isSmallScreen? 30:50 : 10, 
        marginRight: 15 
    },
    headerBox: { 
        paddingRight: 10, 
        paddingLeft:10,
        width: '100%', 
        justifyContent: 'center', 
        backgroundColor: 'rgba(247, 148, 29,1)' 
    },
    txtBox: { marginTop: 10,paddingBottom: Platform.OS==='android'? 10:5,},
    txtTitle: { 
        fontSize: 25,
        fontWeight: 'bold', 
        color: 'white',
        textAlign:'center',
        fontWeight: 'bold' ,
        textShadowColor: 'rgba(0, 0, 0, 0.3)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    },
    listBox: {
        flex: 1, marginTop: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingBottom: 150,
        backgroundColor: 'white'
    },
    listStyle: { width: '100%', padding: 20, borderTopColor: '#eee', borderTopWidth: 0.5, },
    txtStyle: { marginLeft: 20, fontSize: 18, color: '#444444' },
    txtCenter: { padding: 10, paddingLeft: 20, paddingRight: 20, paddingTop: 20, textAlign: 'center' },
    totalBox:{alignSelf:'center',marginTop:-5,backgroundColor:'orange',borderRadius:25,width:25,height:25},
    txtTotal:{
        color:'white',
        fontSize:10,
        textAlign:'center',
        marginTop:5,
        fontWeight:'bold',
        fontWeight: 'bold' ,
        textShadowColor: 'rgba(0, 0, 0, 0.3)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    }
})